from __future__ import print_function
import os
import subprocess


for root, dirs, files in os.walk("output/"):
    for file in files:
        if file.endswith(".pdf"):
            filename = os.path.join(root, file)
            output = os.path.join(root, file + 'o')

            arg1 = '-sOutputFile=' + output

            p = subprocess.Popen([
                '/usr/bin/gs',
                '-sDEVICE=pdfwrite',
                '-dCompatibilityLevel=1.4',
                '-dPDFSETTINGS=/default',
                '-dNOPAUSE',
                '-dBATCH',
                '-dQUIET',
                str(arg1),
                filename
            ], stdout=subprocess.PIPE)
            print(p.communicate())

            os.rename(output, filename)
