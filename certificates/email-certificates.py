import getpass
import glob
import os
import smtplib
import os.path as op

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders


'''Config
'''
subject = "FOSSers - OpenHackUtsav Certificate"
message = '''Hi !

Thank you for participating ! Here's your certificate

- FOSSersVAST Team
'''

smtp_username = 'fossers@vidyaacademy.ac.in'


def send_mail(send_from, send_to, subject, message, files=[],
              server="localhost", port=587, username='', password='',
              use_tls=True):
    '''Compose and send email with provided info and attachments.
    https://stackoverflow.com/a/16509278/1372424

    Args:
        send_from (str): from name
        send_to (list[str]): to name
        subject (str): message title
        message (str): message body
        files (list[str]): list of file paths to be attached to email
        server (str): mail server host name
        port (int): port number
        username (str): server auth username
        password (str): server auth password
        use_tls (bool): use TLS mode
    '''

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(message))

    for path in files:
        part = MIMEBase('application', 'octet-stream')
        with open(path, 'rb') as file:
            part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(op.basename(path)))
        msg.attach(part)

    smtp = smtplib.SMTP(server, port)
    if use_tls:
        smtp.ehlo()
        smtp.starttls()
    smtp.login(username, password)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.quit()


emails = []
for file in glob.glob('output/*.pdf'):
    name = file.replace('output/cert-', '')
    email = name.replace('.pdf', '')

    if email == '':
        continue

    emails.append(email)

print(subject + '\n')
print(message + '\n')
print(emails)
print('\n\n')

c = str(len(emails))

if input('IMPORTANT! Confirm sending mails to ' + c + ' people (Y/n):') != 'Y':
    exit()

print('SMTP Username : ' + smtp_username)
smtp_password = getpass.getpass('SMTP Password : ')

for email in emails:
    file = 'output/cert-' + email + '.pdf'
    print(email + ' - ' + file + ' - sending')
    send_mail(
        'FOSSers VAST',
        [email],
        subject,
        message,
        [file],
        'smtp.gmail.com',
        587,
        smtp_username,
        smtp_password,
        True
    )
