# Certificate Generator

## Requirements

* Python3
* Inkscape

## Process

* Install certg :
  ```
  pip3 install certg --user
  ```
* Make a CSV file like `format.csv` with the name `certificates.csv`
* Convert CSV to YAML and generate `certificates.yaml` :
  ```
  python3 csv-to-yaml.py
  ```
* Edit `certificates.yaml` config file as necessary
* Create folder `output` and make the certificates !
  ```
  mkdir output
  certg certificates.yaml
  ```
  This'll make PDFs in `output` folder
* Compress PDFs which are in `output` folder :
  ```
  python3 compress-pdfs.py
  ```
* Edit `email-certificates.py` and change email subject, body etc.
* Email the certificates :
  ```
  python3 email-certificates.py
  ```